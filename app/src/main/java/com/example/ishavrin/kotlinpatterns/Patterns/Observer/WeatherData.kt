package com.example.ishavrin.kotlinpatterns.Patterns.Observer

open class WeatherData:Subject {
    private val observers = ArrayList<Observer>()
    lateinit var temperature:String
    lateinit var humidity:String
    lateinit var pressure:String


    override fun registerObserver(o:Observer) {
        observers.add(o)
    }

    override fun removeObserver(o:Observer) {
        if(observers.indexOf(o) != -1)
        observers.remove(o)
    }

    override fun notifyObservers() {
        observers.forEach{
            it.update(temperature,humidity,pressure)
        }
    }

    fun measurementsChanged(temp:String, hum:String, press:String) {
        temperature = temp
        humidity = hum
        pressure = press
        notifyObservers()
    }
}