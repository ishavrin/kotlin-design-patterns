package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory

interface Sauce {
    fun getName():String
}