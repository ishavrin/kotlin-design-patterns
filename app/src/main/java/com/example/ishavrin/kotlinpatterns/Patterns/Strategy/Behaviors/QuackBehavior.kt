package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors

interface QuackBehavior {
open fun quack():String
}
