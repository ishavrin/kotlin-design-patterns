package com.example.ishavrin.kotlinpatterns.Patterns.Decorator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee.DarkRoast
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee.Espresso
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee.HouseBlend
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Conditement.Mocha
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Conditement.Soy
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Conditement.Whip
import com.example.ishavrin.kotlinpatterns.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class DecoratorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_observer)
        var s:String = ""
        val beverage:Beverage = Espresso();
        s = s + beverage.getDescription() + " $" + beverage.cost()+"\n"
        var beverage2:Beverage = DarkRoast()
        beverage2 = Mocha(beverage2)
        beverage2 = Mocha(beverage2)
        beverage2 = Whip(beverage2)
        s = s + beverage2.getDescription() + " $" + beverage2.cost()+"\n"
        var beverage3:Beverage = HouseBlend();
        beverage3 = Soy(beverage3)
        beverage3 = Mocha(beverage3)
        beverage3 = Whip(beverage3)
        s = s + beverage3.getDescription() + " $" + beverage3.cost()+"\n"

//Must display 3 times

        verticalLayout {
            lparams(matchParent, matchParent)
            gravity = Gravity.CENTER_HORIZONTAL
            textView("DECORATOR") {

            }.lparams {
                topMargin = dip(16)
            }

            textView(s) {


            }.lparams {
                topMargin = dip(16)
            }

            button("Back") {
                onClick {
                    this@DecoratorActivity.onBackPressed()
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }


        }

    }
}
