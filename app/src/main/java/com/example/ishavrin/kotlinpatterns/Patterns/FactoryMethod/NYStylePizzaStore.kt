package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod

import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.NY.NYStyleCheesePizza
import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.NY.NYStyleClamPizza
import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.NY.NYStylePepperoniPizza

class NYStylePizzaStore:PizzaStore() {
    override fun createPizza(type: String): Pizza {
  when(type){
      "cheese" -> {
          return NYStyleCheesePizza()
      }
      "pepperoni" -> {
       return NYStylePepperoniPizza()
      }
      "clam" -> {
          return NYStyleClamPizza()
      }
  }
        return NYStyleCheesePizza()
  }
}