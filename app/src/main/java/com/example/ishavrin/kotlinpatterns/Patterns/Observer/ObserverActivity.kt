package com.example.ishavrin.kotlinpatterns.Patterns.Observer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import com.example.ishavrin.kotlinpatterns.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class ObserverActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_observer)

        val weatherData = WeatherData()
        val currentConditionsDisplay = CurrentConditionsDisplay(weatherData)
        weatherData.registerObserver(currentConditionsDisplay)
        weatherData.registerObserver(currentConditionsDisplay)
        weatherData.measurementsChanged("100", "101", "102")

//Must display 3 times

        verticalLayout {
            lparams(matchParent, matchParent)
            gravity = Gravity.CENTER_HORIZONTAL
            textView("OBSERVER") {

            }.lparams {
                topMargin = dip(16)
            }

            textView(currentConditionsDisplay.displayString) {


            }.lparams {
                topMargin = dip(16)
            }

            button("Back") {
                onClick {
                    this@ObserverActivity.onBackPressed()
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }


        }

    }
}
