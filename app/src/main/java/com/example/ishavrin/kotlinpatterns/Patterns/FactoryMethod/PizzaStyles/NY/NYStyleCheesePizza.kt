package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.NY

import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.Pizza

class NYStyleCheesePizza: Pizza() {
    val name:String = "NYStyleCheesePizza "
    override fun prepare() {
        pizzaString+=name
        super.prepare()
    }

    override fun bake() {
        pizzaString+=name
        super.bake()
    }

    override fun cut() {
        pizzaString+=name
        super.cut()
    }

    override fun box() {
        pizzaString+=name
        super.box()
    }

}