package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory

interface Dough {
    fun getName():String
}