package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Conditement

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.ConditementDecorator

class Whip(var beverage: Beverage): ConditementDecorator() {
    override fun getDescription(): String {
        return beverage.getDescription()+", Whip";
    }

    override fun cost(): Double {
        return .15 + beverage.cost()
    }
}