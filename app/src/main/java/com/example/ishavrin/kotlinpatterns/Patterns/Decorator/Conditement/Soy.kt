package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Conditement

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.ConditementDecorator

class Soy(var beverage: Beverage): ConditementDecorator() {
    override fun getDescription(): String {
        return beverage.getDescription()+", Soy";
    }

    override fun cost(): Double {
        return .10 + beverage.cost()
    }
}