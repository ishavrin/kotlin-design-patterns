package com.example.ishavrin.kotlinpatterns

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.DecoratorActivity
import com.example.ishavrin.kotlinpatterns.Patterns.Observer.ObserverActivity
import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.FactoryActivity
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.StrategyActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        verticalLayout {
            lparams(matchParent, matchParent)
            gravity = Gravity.CENTER_HORIZONTAL

            textView("PATTERNS:") {

            }.lparams {
                topMargin = dip(16)
            }

            button("STRATEGY") {
                onClick {
                    startActivity(Intent(this@MainActivity, StrategyActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }

            button("OBSERVER") {
                onClick {
                    startActivity(Intent(this@MainActivity, ObserverActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }

            button("DECORATOR") {
                onClick {
                    startActivity(Intent(this@MainActivity, DecoratorActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }

            button("FACTORY") {
                onClick {
                   startActivity(Intent(this@MainActivity, FactoryActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }

            button("ABSTRACTFACTORY") {
                onClick {
                   // startActivity(Intent(this@MainActivity, AbstractFactoryActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }

            button("VISITOR") {
                onClick {
                   // startActivity(Intent(this@MainActivity, VisitorActivity::class.java))
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }
        }
    }
}
