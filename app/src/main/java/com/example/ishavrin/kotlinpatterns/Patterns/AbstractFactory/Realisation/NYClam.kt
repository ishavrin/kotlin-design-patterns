package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Realisation

import com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Clam

class NYClam:Clam {
    override fun getName(): String {
        return "New Yourk clam"
    }
}