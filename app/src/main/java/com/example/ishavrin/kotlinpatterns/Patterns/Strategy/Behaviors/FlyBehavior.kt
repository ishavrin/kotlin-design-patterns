package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors

interface FlyBehavior {
    open fun fly():String
}
