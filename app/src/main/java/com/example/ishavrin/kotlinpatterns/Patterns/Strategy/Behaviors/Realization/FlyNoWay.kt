package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.FlyBehavior

class FlyNoWay:FlyBehavior {
    override fun fly(): String {
        return "I can't fly"
    }

}