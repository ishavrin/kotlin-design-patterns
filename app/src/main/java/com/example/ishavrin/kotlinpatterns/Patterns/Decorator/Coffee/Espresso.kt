package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage

class Espresso: Beverage("Espresso") {
    override fun cost(): Double {
        return 1.99
    }
}