package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Realization

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization.FlyWithWings
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization.Quack
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Duck

class MallardDuck:Duck {


    constructor() {
        mQuackBehavior = Quack()
        mFlyBehavior = FlyWithWings()
    }

    override fun display():String {
        return "I'm really Mallard Duck"
    }
}