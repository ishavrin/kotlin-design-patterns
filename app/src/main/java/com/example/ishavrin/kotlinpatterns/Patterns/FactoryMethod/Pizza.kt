package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod

open class Pizza {
    open var pizzaString:String = ""
    open fun prepare() {
        pizzaString+=" prepare\n"
    }

    open fun bake() {
        pizzaString+=" bake\n"
    }

    open fun cut() {
        pizzaString+=" cut\n"
    }

    open fun box() {
        pizzaString+=" box\n"
    }
}