package com.example.ishavrin.kotlinpatterns.Patterns.Observer

interface DisplayElement {
    fun display():String
}