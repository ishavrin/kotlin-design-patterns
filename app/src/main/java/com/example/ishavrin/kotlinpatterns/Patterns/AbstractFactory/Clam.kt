package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory

interface Clam {
    fun getName():String
}