package com.example.ishavrin.kotlinpatterns.Patterns.Observer

interface Observer {
    fun update(temp:String, hum:String, press:String)
}