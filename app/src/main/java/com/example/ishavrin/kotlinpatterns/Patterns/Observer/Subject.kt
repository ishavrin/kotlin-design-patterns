package com.example.ishavrin.kotlinpatterns.Patterns.Observer

interface Subject {
    fun registerObserver(o:Observer)
    fun removeObserver(o:Observer)
    fun notifyObservers()
}