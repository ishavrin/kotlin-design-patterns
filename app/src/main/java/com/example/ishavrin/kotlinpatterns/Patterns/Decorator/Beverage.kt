package com.example.ishavrin.kotlinpatterns.Patterns.Decorator

abstract class Beverage(open var description:String = "Unknow Bewerage") {

    internal open fun getDescription():String {
        return description
    }
    abstract fun cost():Double
}