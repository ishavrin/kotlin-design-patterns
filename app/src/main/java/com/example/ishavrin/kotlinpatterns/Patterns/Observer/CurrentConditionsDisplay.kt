package com.example.ishavrin.kotlinpatterns.Patterns.Observer

class CurrentConditionsDisplay(weatherData:WeatherData):Observer,DisplayElement {

    init {
        weatherData.registerObserver(this)
    }

    private var temperature:String = ""
    private var humidity:String = ""
    private  var pressure:String = ""
    var displayString:String = ""

    override fun update(temp: String, hum: String, press: String) {
        temperature = temp
        humidity = hum
        pressure = press
        displayString=displayString+display();
    }

    override fun display(): String {
    return "CurrentConditions:\n"+
        temperature+"\n"+
            humidity+"\n"+
            pressure+"\n"

    }
}