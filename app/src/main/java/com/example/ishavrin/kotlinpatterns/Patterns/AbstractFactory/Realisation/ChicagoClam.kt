package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Realisation

import com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Clam

class ChicagoClam:Clam {
    override fun getName(): String {
        return "Chicago Clam"
    }
}