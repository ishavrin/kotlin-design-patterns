package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod

import android.os.Bundle

import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import com.example.ishavrin.kotlinpatterns.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick


class FactoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_factory)

        var nyStore = NYStylePizzaStore()
        var chicagoStore = ChicagoStylePizzaStore()
        var pizza = nyStore.orderPizza("pepperoni")


        var s = pizza.pizzaString + "\n\n"
        pizza = chicagoStore.orderPizza("clam")

        s += pizza.pizzaString+ "\n\n"
        pizza = chicagoStore.orderPizza("asd")
        s += pizza.pizzaString+ "\n\n"


        verticalLayout {
            lparams(matchParent, matchParent)
            gravity = Gravity.CENTER_HORIZONTAL

            textView("FACTORY:") {

            }.lparams {
                topMargin = dip(16)
            }

            textView(s) {




            }.lparams {
                topMargin = dip(16)
            }

            /**
            linearLayout{
                lparams(matchParent, wrapContent)

                textView("Вес:"){
                }.lparams(matchParent, wrapContent,1.3f){
                }

                editText(){
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    singleLine = true
                    nextFocusForwardId = View.generateViewId()

                }.lparams(matchParent, wrapContent, 1.0f) {

                }

                textView("Оси:").lparams(matchParent, wrapContent, 1.3f)
                editText(){
                id = View.generateViewId()
                }.lparams(matchParent, wrapContent, 1.0f)
            }
            linearLayout{
                lparams(matchParent, wrapContent)
                textView("Нажат:")
                editText(){

                }
                textView("РТ100:")
                editText(){

                }
            }



            textView("PATTERNS:") {

            }
                    .lparams {
                topMargin = dip(16)
            }

            editText() {
                textChangedListener {


                }
                addTextChangedListener(object: TextWatcher {
                    override fun afterTextChanged(displayString: Editable?) {
                        if (displayString.toString().contains("end"))
                            this@StrategyActivity.onBackPressed()
                    }

                    override fun beforeTextChanged(displayString: CharSequence?, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(displayString: CharSequence?, start: Int, before: Int, count: Int) {

                    }

                })
            }
**/

            button("Back") {
                onClick {
                    this@FactoryActivity.onBackPressed()
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }
        }
    }

}
