package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory

interface PizzaIngredientFactory {
    fun createDough():Dough
    fun createCheese():Cheese
    fun createClam():Clam
    fun createSauce():Sauce
}