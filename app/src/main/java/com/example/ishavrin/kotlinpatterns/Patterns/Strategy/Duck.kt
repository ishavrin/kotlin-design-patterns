package com.example.ishavrin.kotlinpatterns.Patterns.Strategy

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.FlyBehavior
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.QuackBehavior

abstract class Duck() {
    lateinit var mFlyBehavior: FlyBehavior
    lateinit var mQuackBehavior: QuackBehavior
    open abstract fun display():String
    open fun performFly():String {
        return mFlyBehavior.fly()
    }

    open fun performQuack():String {
        return mQuackBehavior.quack()
    }

    open fun swim():String {
        return "All Ducs swim"
    }

    open fun setFlyBehavior(flyBehavior: FlyBehavior){
        this.mFlyBehavior = flyBehavior
    }

    open fun setQuackBehavior(quackBehavior: QuackBehavior) {
        this.mQuackBehavior = quackBehavior
    }

}