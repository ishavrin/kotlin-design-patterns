package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.FlyBehavior

class FlyWithWings:FlyBehavior {
    override fun fly(): String {
        return "I'm flying"
    }
}