package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod

abstract class PizzaStore {
    fun orderPizza(type:String):Pizza {
        var pizza:Pizza;

        pizza = createPizza(type)
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()

    return pizza
    }

    abstract fun createPizza(type: String): Pizza
}