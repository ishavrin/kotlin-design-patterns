package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.NY

import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.Pizza

class NYStylePepperoniPizza: Pizza() {
    val name:String = "NYStylePepperoniPizza "
    override fun prepare() {
        pizzaString+=name
        super.prepare()
    }

    override fun bake() {
        pizzaString+=name
        super.bake()
    }

    override fun cut() {
        pizzaString+=name
        super.cut()
    }

    override fun box() {
        pizzaString+=name
        super.box()
    }

}