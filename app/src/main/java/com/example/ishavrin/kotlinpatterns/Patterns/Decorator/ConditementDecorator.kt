package com.example.ishavrin.kotlinpatterns.Patterns.Decorator

abstract class ConditementDecorator: Beverage() {
    abstract override fun getDescription():String
}