package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.Chicago

import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.Pizza

class ChicagoStylePepperoniPizza: Pizza() {
    val name:String = "ChicagoStylePepperoniPizza "
    override fun prepare() {
        pizzaString+=name
        super.prepare()
    }

    override fun bake() {
        pizzaString+=name
        super.bake()
    }

    override fun cut() {
        pizzaString+=name
        super.cut()
    }

    override fun box() {
        pizzaString+=name
        super.box()
    }

}