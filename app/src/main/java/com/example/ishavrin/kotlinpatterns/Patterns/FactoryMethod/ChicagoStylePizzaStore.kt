package com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod

import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.Chicago.ChicagoStyleCheesePizza
import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.Chicago.ChicagoStyleClamPizza
import com.example.ishavrin.kotlinpatterns.Patterns.FactoryMethod.PizzaStyles.Chicago.ChicagoStylePepperoniPizza

class ChicagoStylePizzaStore:PizzaStore() {

    override fun createPizza(type: String): Pizza {
        when(type){
            "cheese" -> {
                return ChicagoStyleCheesePizza()
            }
            "pepperoni" -> {
                return ChicagoStylePepperoniPizza()
            }
            "clam" -> {
                return ChicagoStyleClamPizza()
            }
        }
        return ChicagoStyleCheesePizza()
    }

}