package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory

interface Cheese {
    fun getName():String
}