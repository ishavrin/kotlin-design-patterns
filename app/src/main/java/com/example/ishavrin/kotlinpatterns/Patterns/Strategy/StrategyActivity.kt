package com.example.ishavrin.kotlinpatterns.Patterns.Strategy

import android.content.Intent
import android.os.Bundle

import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization.FlyRocketPowered
import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Realization.MallardDuck
import com.example.ishavrin.kotlinpatterns.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.textChangedListener


class StrategyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_strategy)

        val mallardDuck = MallardDuck()
        var s = (mallardDuck.display()+"\n"
                + mallardDuck.performQuack()+ "\n"
                + mallardDuck.performFly())
        mallardDuck.setFlyBehavior(FlyRocketPowered())
        s += "\n"+mallardDuck.performFly()

        verticalLayout {
            lparams(matchParent, matchParent)
            gravity = Gravity.CENTER_HORIZONTAL

            textView("STRATEGY:") {

            }.lparams {
                topMargin = dip(16)
            }

            textView(s) {




            }.lparams {
                topMargin = dip(16)
            }

            /**
            linearLayout{
                lparams(matchParent, wrapContent)

                textView("Вес:"){
                }.lparams(matchParent, wrapContent,1.3f){
                }

                editText(){
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    singleLine = true
                    nextFocusForwardId = View.generateViewId()

                }.lparams(matchParent, wrapContent, 1.0f) {

                }

                textView("Оси:").lparams(matchParent, wrapContent, 1.3f)
                editText(){
                id = View.generateViewId()
                }.lparams(matchParent, wrapContent, 1.0f)
            }
            linearLayout{
                lparams(matchParent, wrapContent)
                textView("Нажат:")
                editText(){

                }
                textView("РТ100:")
                editText(){

                }
            }



            textView("PATTERNS:") {

            }
                    .lparams {
                topMargin = dip(16)
            }

            editText() {
                textChangedListener {


                }
                addTextChangedListener(object: TextWatcher {
                    override fun afterTextChanged(displayString: Editable?) {
                        if (displayString.toString().contains("end"))
                            this@StrategyActivity.onBackPressed()
                    }

                    override fun beforeTextChanged(displayString: CharSequence?, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(displayString: CharSequence?, start: Int, before: Int, count: Int) {

                    }

                })
            }
**/

            button("Back") {
                onClick {
                    this@StrategyActivity.onBackPressed()
                }
            }.lparams(dip(120)) {
                topMargin = dip(16)
            }
        }
    }

}
