package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.FlyBehavior

class FlyRocketPowered:FlyBehavior {
    override fun fly(): String {
        return "I'm flying with a rocket!"
    }
}