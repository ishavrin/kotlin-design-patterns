package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage

class HouseBlend: Beverage("House Blend Coffee") {
    override fun cost(): Double {
        return .89
    }

}