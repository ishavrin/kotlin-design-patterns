package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Realisation

import com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Cheese

class ChicagoCheese:Cheese {
    override fun getName(): String {
        return "Chicago cheese"
    }
}