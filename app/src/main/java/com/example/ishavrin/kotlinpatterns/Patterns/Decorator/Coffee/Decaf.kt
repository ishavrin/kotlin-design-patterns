package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage

class Decaf: Beverage("Decaf Coffee") {
    override fun cost(): Double {
        return .79
    }

}