package com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Coffee

import com.example.ishavrin.kotlinpatterns.Patterns.Decorator.Beverage

class DarkRoast: Beverage("Dark Roast Coffee") {
    override fun cost(): Double {
        return .99
    }

}