package com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.Realization

import com.example.ishavrin.kotlinpatterns.Patterns.Strategy.Behaviors.QuackBehavior

class Squeak:QuackBehavior {
    override fun quack(): String {
        return "Sqeack"
    }

}