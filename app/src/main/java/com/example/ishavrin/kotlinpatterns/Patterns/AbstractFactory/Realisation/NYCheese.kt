package com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Realisation

import com.example.ishavrin.kotlinpatterns.Patterns.AbstractFactory.Cheese

class NYCheese:Cheese {
    override fun getName(): String {
        return "New York Cheese"
    }
}